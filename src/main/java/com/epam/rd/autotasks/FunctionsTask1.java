package com.epam.rd.autotasks;

import java.util.Arrays;

class FunctionsTask1 {
    /**
     * <summary>
     * Implement code according to description of task.
     * </summary>
     * if set invalid arguments in method, then method must throws
     * IllegalArgumentException
     */
    public static boolean isSorted(int[] array, SortOrder order) {
        // throw new UnsupportedOperationException();
        //This will depend on what you have to return for null condition
        if (array == null || array.length <= 1) {
            return true;
        }
        //Where the condition fpr sortOder is met,we find any element
        // which is greater then its next element and return false.
        // otherwise it will be descending.
        if (order == SortOrder.ASC ) {
            for (int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    return false;
                }

            }
        } else {
            for (int i = 1; i < array.length; i++) {
                if (array[i] > array[i - 1]) {
                    return false;
                }
            }
        }

    return true;
}
}

