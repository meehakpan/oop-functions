package com.epam.rd.autotasks;

import java.util.HashMap;
import java.util.Map;

public enum SortOrder {
    ASC,
    DESC,
}